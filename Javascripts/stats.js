import {gameJson} from "./data/game.js";

const form = document.querySelector('#myForm');
const canvas = document.getElementById('myChart');
let ctx = canvas.getContext('2d');
let myChart = null;

let searchCriteria = {
    game_id: null,
    player_id: null,
    outcome: null,
    player_score: null,
    cpu_score: null,
    date: null
};

let specialSearchCriteria = {
    player_score_value: null,
    cpu_score_value: null,
    before_date: null,
    after_date: null
}

function performSearch() {
    const searchResults = searchItems(gameJson, searchCriteria);
    console.log(searchResults);
    createTable(searchResults);
    setGraphData(searchResults);
}

function clearTable() {
    const tableBody = document.querySelector('#data-table tbody');
    while (tableBody.firstChild) {
        tableBody.removeChild(tableBody.firstChild);
    }
}

function createTable(data){
    clearTable();
    const tableBody = document.querySelector('#data-table tbody');
    data.forEach(item =>{
        const row = document.createElement('tr');
        let outcomeClass = '';
        if (item.outcome === 'WIN') {
            outcomeClass = 'win';
        } else if (item.outcome === 'DRAW') {
            outcomeClass = 'draw';
        } else if (item.outcome === 'LOSS') {
            outcomeClass = 'lose';
        } else if (item.outcome === 'IN-PROGRESS') {
            outcomeClass = 'in-progress';
        }
        row.innerHTML = `<td class="game_id center">${item.game_id}</td>
                      <td class="center">${item.player_id}</td>
                      <td>${item.group_type}</td>
                      <td>${item.bonus}</td>
                      <td class="center">${item.player_score}</td>
                      <td class="center">${item.cpu_score}</td>
                      <td class="${outcomeClass}">${item.outcome}</td>
                      <td>${item.date}</td>
                      <td class="center">${convertMillisecondsToSeconds(item.average_time)}</td>`;
        tableBody.appendChild(row);
    })
}

// Add event listener to the form for submit event
form.addEventListener('submit', function(event) {
    event.preventDefault();

    // Get the input values
    const gameIDInput = document.querySelector('#game_id');
    const playerIDInput = document.querySelector('#player_id');
    const playerScoreValueInput = document.querySelector('#player_score_value')
    const playerScoreInput = document.querySelector('#player_score')
    const cpuScoreValueInput = document.querySelector('#cpu_score_value')
    const cpuScoreInput = document.querySelector('#cpu_score')
    const progressInput = document.querySelector('#progress')
    const beforeDateInput = document.querySelector('#before_date')
    const afterDateInput = document.querySelector('#after_date')

    searchCriteria.game_id = parseInt(gameIDInput.value) || null;
    searchCriteria.player_id = parseInt(playerIDInput.value) || null;
    searchCriteria.outcome = progressInput.value || null;
    searchCriteria.player_score = parseInt(playerScoreInput.value) || null;
    searchCriteria.cpu_score = parseInt(cpuScoreInput.value) || null;

    if(beforeDateInput.value || null) searchCriteria.date = true;
    if(afterDateInput.value || null) searchCriteria.date = true;

    specialSearchCriteria.before_date = beforeDateInput.value || null;
    specialSearchCriteria.after_date = afterDateInput.value || null;
    specialSearchCriteria.player_score_value = playerScoreValueInput || null;
    specialSearchCriteria.cpu_score_value = cpuScoreValueInput || null;

    performSearch();
});

function searchItems(data, searchCriteria) {
    return data.filter(item => {
        // Compare item properties against search criteria
        return Object.keys(searchCriteria).every(key => {
            if (searchCriteria[key] === null) {
                // If the criteria is null, don't filter based on this property
                return true;
            }
            else if(searchCriteria[key] === searchCriteria.date && searchCriteria.date === true){
                if(specialSearchCriteria.before_date === null){
                    return item[key] > specialSearchCriteria.after_date
                } else if(specialSearchCriteria.after_date === null){
                    return item[key] < specialSearchCriteria.before_date
                } else{
                    return item[key] < specialSearchCriteria.before_date && item[key] > specialSearchCriteria.after_date
                }
            }
            else if(searchCriteria[key] === searchCriteria.player_score || searchCriteria[key] === searchCriteria.cpu_score){
                switch (searchCriteria[key]){
                    case searchCriteria.player_score:
                        if(specialSearchCriteria.player_score_value.value === null){
                            return item[key] === searchCriteria[key];
                        }
                        else{
                                switch (specialSearchCriteria.player_score_value.value){
                                    case "greater":
                                        return item[key] > searchCriteria[key];
                                    case "equals":
                                        return item[key] === searchCriteria[key];
                                    case "less":
                                        return item[key] < searchCriteria[key];
                                }
                        }
                        break;
                    case searchCriteria.cpu_score:
                        if(specialSearchCriteria.cpu_score_value.value === null){
                            return item[key] === searchCriteria[key];
                        }
                        else{
                            switch (specialSearchCriteria.cpu_score_value.value){
                                case "greater":
                                    return item[key] > searchCriteria[key];
                                case "equals":
                                    return item[key] === searchCriteria[key];
                                case "less":
                                    return item[key] < searchCriteria[key];
                            }
                        }
                        break;
                }
            }
            else {
                return item[key] === searchCriteria[key];
            }
        });
    });
}


function setGraphData(data) {
    let dataAverageTime = [];
    let dataLabels = [];

    data.forEach(item => {
        dataLabels.push('Game ID ' + item.game_id);
        dataAverageTime.push(convertMillisecondsToSeconds(item.average_time));
    });

    // Destroy the existing chart instance if it exists
    if (myChart) {
        myChart.destroy();
    }

    // Define the data for the chart
    let graphData = {
        labels: dataLabels,
        datasets: [{
            label: 'Average Tune',
            data: dataAverageTime,
            borderColor: 'rgba(54, 162, 235, 1)',
            backgroundColor: 'rgba(54, 162, 235, 0.2)',
            fill: true
        }]
    };

    // Create the chart instance
    myChart = new Chart(ctx, {
        type: 'line',
        data: graphData,
        options: {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}



//Convert time
function convertMillisecondsToSeconds(milliseconds) {
    return milliseconds / 1000;
}

//To Sort data
window.onload = function (){
    gameJson.sort((a, b) => a.game_id - b.game_id);
}
