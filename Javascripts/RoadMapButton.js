function scrollToContent() {
    const contentElement = document.querySelector('.content');
    contentElement.scrollIntoView({ behavior: 'smooth' });
}