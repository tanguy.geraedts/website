import {usersJson} from "./data/users.js";

const form = document.querySelector('#loginForm');

form.addEventListener('submit', function(event) {
    event.preventDefault(); // Prevent form submission

    // Get input values
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    checkCredentials(username,password)
});


function checkCredentials(username, password) {

    let user = usersJson.find(user => user.username === username && user.password === password);
    if (user) {
        console.log('Login successful');
        hideLogin()
        if(hasRole(user.role, 'admin') === true){
            showStats()
        } else{
            showNoPerms()
        }
    } else {
        showInvalidLogin()
        console.log('Invalid username or password');
    }
}


function hasRole(userRole, requiredRole) {
    return userRole === requiredRole;
}


function showNoPerms(){
    var div = document.getElementById('noPerm');
    div.style.display = 'block';
}

function showInvalidLogin(){
    var div = document.getElementById('login_success');
    div.style.display = 'block';
}
function showStats(){
    var div = document.getElementById('gameStats');
    div.style.display = 'block';
}

function hideLogin(){
    var div = document.getElementById('login');
    div.style.display = 'none';
}


